<?php
/**
 * This file is part of the framework
 *
 * The ForumController class is used to prevent default actions
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class ForumController extends KernelController {
    /**
     * --------------------------------------------------
     * CONSTANTS
     * --------------------------------------------------
     */
    const PAGE_ID = 'forum';
    const PAGE_TITLE = 'Forum';

    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init( __FILE__, __FUNCTION__, $db ); // Adds third paramater for database usage

        try {
            $forum = new ClassForum( $this->getModel()->getConversation() );
        } catch( ForumException $e ) {
            die( $e );
        } catch( Exception $e ) {
            die( $e->getMessage() );
        }

        $this->setProperty( 'title', self::PAGE_TITLE );
        $this->setProperty( 'ariane', $this->ariane( _( self::PAGE_TITLE ) ) );
        $this->setProperty( 'forum', $forum );
        $this->render( true );
    }

    /**
     * conversationAction
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function conversationAction( PDO $db = null ) {
        $this->init( __FILE__, __FUNCTION__, $db ); // Adds third paramater for database usage

        $walks = NavigationManagement::walks( DOMAIN );
if( isset($walks[count($walks)-1]) && $walks[count($walks)-1]=='?' )
    unset( $walks[count($walks)-1] );

        if( count($walks)>3 ) :
            $conv = $walks[count($walks)-2];
            $pagination = $walks[count($walks)-1];
        else :
            $conv = $walks[count($walks)-1];
            $pagination = 1;
        endif;

        if( !( $conv!==NULL && is_numeric( $conv ) ) )
            NavigationManagement::redirect();

        try {
            ClassConversation::setLimit( 20 );
            ClassConversation::setPagination( $pagination );
            $forum = new ClassForum( $this->getModel()->getConversation( $conv, ClassMessage::getSort() ) );

            if( !( $forum->getConversation( $conv )!==NULL && is_object( $forum->getConversation( $conv ) ) && get_class( $forum->getConversation( $conv ) )=='ClassConversation' ) )
                NavigationManagement::redirect();

        } catch( ForumException $e ) {
            die( $e );
        } catch( Exception $e ) {
            die( $e->getMessage() );
        }

        $this->setProperty( 'title', self::PAGE_TITLE );
        $this->setProperty( 'ariane', $this->ariane( _( self::PAGE_TITLE ) ) );
        $this->setProperty( 'forum', $forum->getConversation( $conv ) );
        $this->render( true );
    }
}