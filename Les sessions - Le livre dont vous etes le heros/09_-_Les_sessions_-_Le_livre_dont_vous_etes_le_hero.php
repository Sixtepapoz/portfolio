<?php
session_start();

require('story.php');

if (!isset($_SESSION['livre']['history'])) {
    $_SESSION['livre']['history'] = array();
}

if (!isset($_SESSION['livre']['index'])) {
    $_SESSION['livre']['index'] = 0;
}

if (isset($_POST['valid'])) {
    if ($_POST['valid'] == 'recommencer') {
    unset($_SESSION['livre']['history']);
    header('location:09_-_Les_sessions_-_Le_livre_dont_vous_etes_le_hero.php');
    exit();
    }
}

if (isset($_POST['clic'])) {
    $_SESSION['livre']['index'] = $_POST['clic'];
}
if (count($_SESSION['livre']['history']) == 0) {
    $_SESSION['livre']['history'][] = $story[$_SESSION['livre']['index']]['text'];
}elseif( $_SESSION['livre']['history'][count($_SESSION['livre']['history'])-1] != $story[$_SESSION['livre']['index']]['text'] ) {
        $_SESSION['livre']['history'][] = $story[$_SESSION['livre']['index']]['text'];
    }

function affiche(&$history){
    foreach ($history as $key => $value) {
        echo '<p>' . $value . '</p>';
    }
}

function choix($story, &$index){
    foreach ($story[$index]['choice'] as $key => $value){
        echo '<button type="submit" name="clic" value= "' . $story[$index]['choice'][$key]['goto'] . '">' . $story[$index]['choice'][$key]['text'] . '</button>';
    }
    echo '<br><br><input type="submit" name="valid" value ="recommencer">';
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0" />
        <title>Le livre dont vous êtes le héro | Les sessions - Mise en pratique</title>
    </head>
    <body style="background-color: grey;">
        <h1>Le livre dont vous êtes le héro | Les sessions - Mise en pratique</h1>
        <p><em>Le livre dont vous êtes le héro est un concept bien connu dans lequel il existe plusieurs points d'arrêt où un choix vous est proposé. Ce choix influence la suite de votre parcours dans l'histoire.</em></p>
        <p><em>Dans cet exercice, le fichier <a href="story.php" title="Morceaux de l'hitoire">story.php</a> contenant les différents morceaux de l'histoire vous est mis à disposition.<br />Il vous est demandé :</em></p>
        <ol style="font-style:italic;">
            <li>de créer une fonction pour afficher le chapitre n</li>
            <li>mettre en place un formulaire proposant les choix possibles à chaque décision à prendre</li>
            <li>faire en sorte d'ajouter une persistance des données pour ne pas perdre le cours de l'histoire</li>
        </ol>
        <hr>
        <?php
            affiche($_SESSION['livre']['history']);
        ?>
        <hr>
        <form class="form" action="09_-_Les_sessions_-_Le_livre_dont_vous_etes_le_hero.php" method="post" accept-charset="utf-8">
            <?php
            choix($story, $_SESSION['livre']['index']);
            ?>
        </form>
    </body>
</html>