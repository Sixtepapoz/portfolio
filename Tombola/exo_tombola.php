<?php
session_start();

if (isset($_GET['reset'])) {
    unset($_SESSION['tombola']);
}


function resultat($nbreticket, $joueur, &$cagnotte, $prix){
    for ($i=0; $i < 3; $i++) {
        $temp[] = mt_rand(1,$nbreticket);
                if (in_array( $temp[$i], $joueur ) ) {
                    $cagnotte = $cagnotte + $prix[$i];
                }
    }
    return $temp;
}

//Remplir un tableau avec des valeurs croissantes en fonction d'un nombre d'entrée
function table(&$tableau, $nbreticket){
    for ($i=0; $i < $nbreticket; $i++) {
        $tableau[$i] = $i+1;
    }
}

function tirage($tableau, $quantity){
    $joueur = (array)array_rand($tableau, $quantity);
    $tickets = array();
    foreach ($joueur as $valueKey) {
        $tickets[] = $tableau[$valueKey];
        unset($tableau[$valueKey]);
    }
    return $tickets;
}

//Gère la cagnotte du joueur en fonction de ses achats
function buy(&$cagnotte, $ticket_price, $quantity){
    if (isset($quantity) && $quantity != "") {
        $cagnotte = $cagnotte - ($ticket_price * $quantity);
    } else $quantity = 0;
}

function tablesimple(&$joueur){
    foreach ($joueur as $key => $value) {
        array_merge_recursive($joueur);
    }
}
//---------------------------------------------------------------------------------------------------


$cagnotte_init = 500;
$_SESSION['tombola']['ticket_price'] = 2;
$_SESSION['tombola']['nbreticket'] = 100;
$_SESSION['tombola']['prix'][0] = 100;
$_SESSION['tombola']['prix'][1] = 50;
$_SESSION['tombola']['prix'][2] = 20;


if (isset($_GET['tirage'])) {
    $_SESSION['tombola']['tirage'] = $_GET['tirage'];
    $_SESSION['tombola']['onglet'] = $_GET['tirage'];
    header('Location: exo_tombola.php');
    exit();
}

if (!isset($_SESSION['tombola']['dispo'])) {
    $_SESSION['tombola']['dispo'] = $_SESSION['tombola']['nbreticket'];
}

if (( isset($_POST['quantity']) && $_POST['quantity'] > $_SESSION['tombola']['dispo']) ) {
    $_POST['quantity'] = 0;
    $_GET['error'] = 1;
}

if (!isset($_SESSION['tombola']['resul_aff'])) {
    $_SESSION['tombola']['resul_aff'][0] = "";
    $_SESSION['tombola']['resul_aff'][1] = "";
    $_SESSION['tombola']['resul_aff'][2] = "";
}

if (isset($_SESSION['tombola']['tirage']) && isset($_SESSION['tombola']['joueur'])) {
    $_SESSION['tombola']['resul_aff'] = resultat($_SESSION['tombola']['nbreticket'], $_SESSION['tombola']['joueur'],$_SESSION['tombola']['cagnotte'] ,$_SESSION['tombola']['prix'] );
}


if (!isset($_SESSION['tombola']['ticket_buy'])) {
    $_SESSION['tombola']['ticket_buy'] = 0;
}

if (!isset($_SESSION['tombola']['tab_ticket'])) {
    $_SESSION['tombola']['tab_ticket'] = array();
}


if (!isset($_SESSION['tombola']['cagnotte'])) {
    $_SESSION['tombola']['cagnotte'] = $cagnotte_init;
}

if (isset($_POST['quantity']) && $_POST['quantity']>0) {
    foreach (tirage($_SESSION['tombola']['tab_ticket'], $_POST['quantity']) as $key => $value) {
        $_SESSION['tombola']['joueur'][] = $value;
    }
    tablesimple($_SESSION['tombola']['joueur']);
    buy($_SESSION['tombola']['cagnotte'], $_SESSION['tombola']['ticket_price'], $_POST['quantity']);
    $_SESSION['tombola']['dispo'] = $_SESSION['tombola']['dispo'] - $_POST['quantity'];
}

if (isset($_SESSION['tombola']['total'])) {
    $_SESSION['tombola']['total'] = $_SESSION['tombola']['nbreticket'] - $_SESSION['tombola']['dispo'];
}else $_SESSION['tombola']['total'] = 0;

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Tombola</title>
</head>
<body style="background-color: lightgrey;">
    <h1 style="text-align: center; font-size: 50px;">TOMBOLA</h1>
    <hr>
    <p><b>Cagnotte initiale :</b>      <?php echo $cagnotte_init; ?> €<br>
       <b>Cagnotte actuelle :</b>      <?php echo '<span style="background-color: lightgreen; border-radius: 3px;"><b>' . $_SESSION['tombola']['cagnotte']; ?> €</span>
    </p>
    <hr>
    <p><b>Nombre de ticket(s) disponibles(s) : </b> (<?php if (isset($_SESSION['tombola']['dispo'])) {echo $_SESSION['tombola']['dispo'];}else echo $_SESSION['tombola']['nbreticket'];  ?>/100)
<?php
    table($_SESSION['tombola']['tab_ticket'], $_SESSION['tombola']['nbreticket']);
    if (isset($_GET['error']) && $_GET['error'] == "1") {
        echo '<hr><div style=" text-align: center;"><span style=" border-radius: 5px; background-color: red; color: white; text-align:center; font-size: 20px; "><b>Il ne reste pas assez de ticket.</span></div>';
    }
?>
    <hr>
    </p>
    <p>Nombre de ticket(s) à acheter :<br>
    <em>(coût par ticket : <?php echo $_SESSION['tombola']['ticket_price']; ?> euros)</em></p>
    <form action="http://localhost/www/exo_tombola.php" method="post" accept-charset="utf-8">
        <input type="number" name="quantity" <?php if (isset($_SESSION['tombola']['onglet'])){ echo 'disabled=""'; } ?> value="" placeholder="<?php echo $_SESSION['tombola']['total'] . '/' . $_SESSION['tombola']['nbreticket']; ?>">
        <input style="background-color: green; border-radius: 20px; font-style: bolder; color: white;" type="submit" name="valid" <?php if (isset($_SESSION['tombola']['onglet'])){ echo 'disabled=""'; } ?> value="$ Acheter $">
    </form>
    <?php
    if (isset($_SESSION['tombola']['joueur'])){
        $i ='';
        echo '<hr><ul style="display: inline-block; width: 10%">';
        foreach ($_SESSION['tombola']['joueur'] as $value){
            echo '<li>' . $value . '</li>';
            $i++;
            if ($i%15 == 0){
                echo '</ul><ul style="display: inline-block; width: 10%">';
            }
        }
        echo '</ul>';
    }
    ?>
    <hr>
    <p style="text-align: center;">
        <?php if (!isset($_SESSION['tombola']['onglet'])) {
            echo '<a href="?tirage"><button style="background-color: Lightgreen; border-color: yellow; border-radius: 50px; height:50px; width: 200px; font-size: 20px;">Tirage au sort</button></a>';
        } ?>
    </p>
    <p style="text-align: center;">
        <a href="?reset" title=""><button style="background-color: red; border-radius: 50px;">Réinitialiser</button></a>
    </p>
    <hr>
    <p style="text-align: center;"><span style="text-decoration: underline;">Résultats du tirage</span>
    <ul style="border: 1px; border-color: yellow; border-style: solid; background-color: gold;">
<?php
        if (isset($_SESSION['tombola']['joueur']) && in_array($_SESSION['tombola']['resul_aff'][0], $_SESSION['tombola']['joueur'])) {
    echo '<li><span style="background-color: green;">Premier prix : ' . $_SESSION['tombola']['resul_aff'][0] . ' Gain de ' . $_SESSION['tombola']['prix'][0] . '$</span></li>';
}else echo '<li>Premier prix : ' . $_SESSION['tombola']['resul_aff'][0] . '</li>';
        if (isset($_SESSION['tombola']['joueur']) && in_array($_SESSION['tombola']['resul_aff'][1], $_SESSION['tombola']['joueur'])) {
    echo '<li><span style="background-color: green;">Deuxième prix : ' . $_SESSION['tombola']['resul_aff'][1] . ' Gain de ' . $_SESSION['tombola']['prix'][1] . '$</span></li>';
}else echo '<li>Deuxième prix : ' . $_SESSION['tombola']['resul_aff'][1] . '</li>';
        if (isset($_SESSION['tombola']['joueur']) && in_array($_SESSION['tombola']['resul_aff'][2], $_SESSION['tombola']['joueur'])) {
    echo '<li><span style="background-color: green;">Troisième prix : ' . $_SESSION['tombola']['resul_aff'][2] . ' Gain de ' . $_SESSION['tombola']['prix'][2] . '$</span></li>';
}else echo '<li>Troisième prix : ' . $_SESSION['tombola']['resul_aff'][2] . '</li>';
?>
    </ul>
    </p>
</body>
</html>

<?php unset( $_SESSION['tombola']['tirage'] );