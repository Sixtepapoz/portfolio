<?php

if (isset($_GET['regen'])){
    header('Location: exo_discours_politique.php');
    exit();
}

if( !defined( 'PHP_EOL' ) ) {
    if( strtoupper( substr( PHP_OS, 0, 3 ) )=='WIN' ) {
        define( 'PHP_EOL', "\r\n" );
    } else {
        define( 'PHP_EOL', "\n" );
    }
}
$fichier = 'data.txt';

$ressource = fopen($fichier, 'r');

$i = 0;

while (fgets($ressource) != PHP_EOL) {
    $i = $i+1;
}

rewind($ressource);

$discours = array();

for ($k=0; $k < 4; $k++) {
    for ($j=0; $j < $i; $j++) {
        $discours[$k][] = fgets($ressource);
    }
    fgets($ressource);
}

/**
 * [intro va chercher et replacer en début de tableau une suite de caractère]
 * @param  [int]    $i          [nbre de ligne par paragraphe]
 * @param  [array]  $discours   [tableau de données des paragraphes]
 * @return [array]              [tableau avec intro en tête]
 */
function intro($i, &$discours){
    $intro = 0;
    $temp = '';
    for ($h=0; $h < $i; $h++) {
        if ($discours[0][$h] == "Mesdames, messieurs," . PHP_EOL) {
            $intro = $h;
        }
    }
    $temp = $discours[0][$intro];
    $discours[0][$intro] = $discours[0][0];
    $discours[0][0] = $temp;
}

/**
 * [melange va mélanger les choix par section dans des tableaux à 2 dimensions]
 * @param  [int]    $i          [nbre de ligne par paragraphe]
 * @param  [array]  $discours   [tableau de données des paragraphes]
 * @return [array]              [tableau mélangé]
 */
function melange($i, &$discours){
    for ($j=0; $j < 4; $j++) {
         shuffle($discours[$j]);
    }
}

/**
 * [affiche gère l'affichage du tableau rendu]
 * @param  [int]    $i          [nbre de ligne par paragraphe]
 * @param  [array]  $discours   [tableau de données des paragraphes]
 * @return [array]              [tableau mélangé]
 */
function affiche($i, &$discours){
    for ($k=0; $k < $i; $k++) {
        echo "<p>";
        for ($j=0; $j < 4; $j++) {
            echo $discours[$j][$k];
            if ($j == 0 && $k == 0) {
                echo "<br>";
            }
        }
        echo "</p>";
    }
}

/**
 * [rendu se résume à compiler les trois fonctions qui la compose]
 * @param  [int]    $i          [nbre de ligne par paragraphe]
 * @param  [array]  $discours   [tableau de données des paragraphes]
 * @return [array]              [affichage mélangé avec en-tête organisée]
 */
function rendu($i, $discours){
    melange($i, $discours);
    intro($i, $discours);
    affiche($i, $discours);
}
fclose( $ressource );
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Politique</title>
</head>
<body style="background-image: url(../../10_-_La_gestion_de_fichiers_-_Politique/parcheminbg.jpg); background-repeat: no-repeat; background-size: cover">
    <h1 style="text-align: center; font-size: 30px; text-decoration: underline;">Discours Politique</h1>
    <div style="border: 2px solid #EED153; width: 50%; background-color: #FFF0BC; margin: auto; padding: 20px; ">
        <p>
            <?php
                rendu($i, $discours);
            ?>
        </p>
    </div >
        <a href="?regen" style="text-decoration: none; margin: auto; white-space:nowrap; text-align: center;" >
            <div style="font-size: 1.2rem; font-weight: bold; color: saddlebrown; border: 2px solid #EED153; width: 15%; background-color: #CCAA62; margin: auto; padding: 20px; margin-top: 5vh; ">
            Générer un nouveau discours
            </div>
        </a>
</body>
</html>