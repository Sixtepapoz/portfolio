<?php

session_start();



/**
 * [liste permet de transférer des variables vers une liste]
 * @param  [string] $produit  [nom de produit]
 * @param  [int] $quantite [quantité de produit]
 * @param  [array] [tableau cible de l'enregistrement]
 * @return [array]           [liste remplie des données d'entrée]
 */
function liste( $user, &$liste){
    $liste = $user;
}


if (isset($_POST['ident']) && isset($_POST['pass'])) {
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );

        if ( ( $reponse = $bdd->prepare ( 'SELECT user.*, r_libelle AS rang FROM `user`
                                            INNER JOIN rang ON user.r_id = rang.r_id;
                                            WHERE `u_login` =:login AND `u_pass`=:pass ;
            ' )  ) !== false ) {
            if ( $reponse->bindValue( "login", $_POST['ident'] ) && $reponse->bindValue( "pass", $_POST['pass']) ) {
                if ( $reponse->execute( ) ) {
                    if ( ( $donnees = $reponse->fetch( PDO::FETCH_ASSOC ) ) !==false ) {
                        if ( ($_POST['ident'] == $donnees['u_login']) && ($_POST['pass'] == $donnees['u_pass'] ) ) {
                           liste($donnees, $_SESSION['membre']);
                        }else{
                            $_SESSION['wrongpass'] = "wrong";
                            header('location:exo_session_membre_form.php');
                            exit();
                        }
                    }
                }
            }
            $reponse->closeCursor( );
        }
    } catch( PDOException $e ) {
        die( $e->getMessage( ) );
    }
}



//Test de la valeur deco, active en cas de clic sur l'onglet déconnexion.
if (isset($_GET['deco'])) {
    if ($_GET['deco'] == "valid") {
        unset($_SESSION['membre']);
        header('location:exo_session_membre_form.php');
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Page membre</title>
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
</head>
<body>
    <h1 style="text-align: center;">Espace membre</h1>
    <hr>
    <h2 style="text-align: center;">Bonjour <?php echo $_SESSION['membre']['u_prenom'] . '<br>' . $_SESSION['membre']['u_nom'] ?></h2>
    <hr>
        <?php
        if ($_SESSION['membre']['rang'] == 'superadmin') {
            echo "<h3>Je suis SuperAdmin/Dieu et c'est bien!</h3>";
        }
        if ($_SESSION['membre']['rang'] == 'admin') {
            echo "<h3>Je suis Admin et bientôt Kalif!</h3>";
        }
        if ($_SESSION['membre']['rang'] == 'invite') {
            echo "<h3>Je suis ...[Null]... et c'est dommage!</h3>";
        }
        ?>
        <a class="button" href="exo_session_membre_ajout.php">Ajouter un utilisateur</a>
        <a class="button" href="exo_session_membre_find.php">Modifier/Supprimer un compte utilisateur</a>
        <a class="button" href="exo_session_membre_mixdel.php">Supprimer plusieurs comptes utilisateurs</a>
        <a class="button" href="?deco=valid">Déconnexion</a>
</body>
</html>