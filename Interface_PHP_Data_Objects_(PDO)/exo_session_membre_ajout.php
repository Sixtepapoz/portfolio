<?php

session_start();




if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['mail']) && isset($_POST['login']) && isset($_POST['pass']) && isset($_POST['rang']) && isset($_POST['age'])) {
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );
        if ( ( $reponse = $bdd->prepare ( 'SELECT user.* FROM `user`
                                            WHERE `u_login` =:login;
            ' )  ) !== false ) {
            if ( $reponse->bindValue( "login", $_POST['login'] )) {
                if ( $reponse->execute( ) ) {
                    if ( ( $donnees = $reponse->fetchAll( PDO::FETCH_ASSOC ) ) !==false ) {
                        if (empty($donnees)) {

                            if ( ( $reponse = $bdd->prepare ( 'INSERT INTO `user` (u_nom, u_prenom, u_mail, u_login, u_pass, u_age, r_id)
                                                               VALUES (:nom, :prenom, :mail, :login, :pass, :age, :rang );
                                ' )  ) !== false ) {
                                if ( $reponse->bindValue("nom", $_POST['nom']) && $reponse->bindValue("prenom", $_POST['prenom']) && $reponse->bindValue("mail", $_POST['mail']) && $reponse->bindValue("login", $_POST['login']) && $reponse->bindValue("pass", $_POST['pass']) && $reponse->bindValue("age", $_POST['age']) && $reponse->bindValue("rang", $_POST['rang'])){
                                    if ( $reponse->execute( ) ) {
                                        $_POST['valid'] = 'valid';
                                    }
                                }
                            }
                        }   else {
                                $_SESSION['wrongpass'] = "wrong";
                                header('location:exo_session_membre_ajout.php');
                                exit();
                            }
                    }
                }
            }
            $reponse->closeCursor( );
    }
    } catch( PDOException $e ) {
        die( $e->getMessage( ) );
    }
}




?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
    <title>Ajout d'utilisateur</title>
</head>
<body>
    <h1>ESPACE SUPERADMIN</h1>
    <hr>
    <h2>Ajout d'un compte</h2>
    <hr>
    <div class="cadre">
        <form class="form" action="" method="post">
            <input type="text" name="nom" placeholder="Nom *" id="nom">
            <input type="text" name="prenom" placeholder="Prénom *">
            <input type="email" name="mail" placeholder="Email de contact *">
            <input type="text" name="login" placeholder="Login *">
            <input type="password" name="pass" placeholder="Mot de passe *">
            <br>
            <label for="birth">Date de naissance *</label>
            <input  id="birth" type="date" name="age" >
            <br>
            <SELECT name="rang" size="1">
                <OPTION selected></option>
<?php
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );
        if ( ( $reponse = $bdd->query ( 'SELECT rang.* FROM `rang`;' )  ) !== false ) {
            while (( $donnees = $reponse->fetch( PDO::FETCH_ASSOC ) ) !==false) {
                echo '<OPTION value="' . $donnees['r_id'] . '">' . $donnees['r_libelle']  . '</OPTION>' ;
            }
        $reponse->closeCursor( );
        }
    }   catch( PDOException $e ) {
        die( $e->getMessage( ) );
        }
?>

            </SELECT>
            <br>
            <label for="nom"> * Champs obligatoires</label>
            <br>
            <input class="button" type="submit" name="Inscrire" value="Inscrire">
            <?php
                if (isset($_POST['valid']) && $_POST['valid'] == 'valid') {
                    echo "<br> Enregistrement effectué";
                    unset($_POST['valid']);
                }
                if (isset($_SESSION['wrongpass'])) {
                    if ($_SESSION['wrongpass'] == 'wrong') {
                        echo '<br>Login déjà existant';
                        unset($_SESSION['wrongpass']);
                    }
                }
            ?>
        </form>
    </div>
    <form action="exo_session_membre_page.php">
        <input class="button" type="submit" value="retour">
    </form>
</body>
</html>