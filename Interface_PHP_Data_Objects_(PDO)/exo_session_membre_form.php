<?php

session_start();

if (isset($_SESSION['membre'])) {
    header('location:exo_session_membre_page.php');
    exit();
}

?>



<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
    <title>Page connexion</title>
</head>
<body>
    <h1>Connexion à l'espace membre</h1>
    <hr>
    <main>
        <div class="cadre">
                <form class="form" action="exo_session_membre_page.php" method="post" accept-charset="utf-8">
                        <input class="input" type="text" name="ident" value="" placeholder="Identifiant">
                        <br>
                        <br>
                        <input class="input" type="password" name="pass" value="" placeholder="Mot de passe">
                        <br>
                        <br>
                        <input class="input button" type="submit" name="Valid" value="Connexion">
                        <?php
                            if (isset($_SESSION['wrongpass'])) {
                                if ($_SESSION['wrongpass'] == 'wrong') {
                                    echo '<br>Erreur mot de passe ou Identifiant';
                                    unset($_SESSION['wrongpass']);
                                }
                            }
                        ?>
                </form>
        </div>
    </main>
</body>
</html>