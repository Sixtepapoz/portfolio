<?php

session_start();

if (isset($_POST['confirme']) && $_POST['confirme'] === "confirmer") {
    delete($_SESSION['login']);
    unset($_POST['confirme']);
}

/**
 * [delete supprime un utilisateur cible de la base de données]
 * @param  [string] $login [le login de l'utilisateur cible]
 * @return [fct SQL]        [effacement de l'utilisateur cible]
 */
function delete($login){
    if (isset($login) && $login != '') {
        try {
                $bdd = new PDO(
                    'mysql:host=localhost;dbname=exoadministration;charset=utf8',
                    'root',
                    '',
                    array(
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
                    )
                );

                if ( ( $reponse = $bdd->prepare ( 'DELETE FROM `user`
                                                    WHERE `u_login` =:login;
                    ' )  ) !== false ) {
                    if ( $reponse->bindValue( "login", $login ) ){
                        if ( $reponse->execute( ) ) {
                            header('location:exo_session_membre_find.php');
                            exit();
                        }
                    }
                    $reponse->closeCursor( );
                }
        }   catch( PDOException $e ) {
            die( $e->getMessage( ) );
            }
    }
}

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
    <title>Suppression</title>
</head>
<body>
    <span class="cadre">
    <form class="form" action="" method="post">
        <p>Etes-vous certain de vouloir supprimer totalement et <b>définitivement</b> l'utilisateur <b><?php echo  $_SESSION['login'] ?></b></p>
    <input class="button" type="submit" name="confirme" value="confirmer">
    <a class="button" href="exo_session_membre_page.php">Annuler</a>
    </form>
    </span>
</body>
</html>