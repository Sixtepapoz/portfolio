<?php

session_start();

if (isset($_SESSION['login']) && $_SESSION['login'] != '' ) {
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );

        if ( ( $reponse = $bdd->prepare ( 'SELECT user.*, r_libelle AS rang FROM `user`
                                            INNER JOIN rang ON user.r_id = rang.r_id
                                            WHERE `u_login` =:login;
            ' )  ) !== false ) {
            if ( $reponse->bindValue( "login", $_SESSION['login'] ) ) {
                if ( $reponse->execute( ) ) {
                    if ( ( $donnees = $reponse->fetchAll( PDO::FETCH_ASSOC ) ) !==false ) {

                    }
                }
            }
            $reponse->closeCursor( );
        }
    } catch( PDOException $e ) {
        die( $e->getMessage( ) );
    }
}



?>





<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Modification</title>
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
</head>
<body>
    <header style="height: 15vh; background-color: black; text-align: center;" >
        <h1 style="color: white">MODIFICATION(S)</h1>
    </header>
    <main style="height: 70vh; text-align: center;">
        <span class="cadre">
        <form class="form" style="" action="" method="post">
            <label for="nom">Nom:</label>
            <input id="nom" type="text" name="" placeholder="<?php echo   $donnees[0]['u_nom'] ;  ?>">
            <br>
            <label for="prenom">Prénom:</label>
            <input id="prenom" type="text" name="" placeholder="<?php echo   $donnees[0]['u_prenom']  ;  ?>">
            <br>
            <label for="age">Né(e) le:</label>
            <input id="age" type="number" name="" placeholder="<?php echo   $donnees[0]['u_age']  ;  ?>">
            <br>
            <label for="mail">Email:</label>
            <input id="mail" type="text" name="" placeholder="<?php echo   $donnees[0]['u_mail']  ;  ?>">
            <br>
            <label for="login">Login:</label>
            <input id="login" type="text" name="" placeholder="<?php echo   $donnees[0]['u_login']  ;  ?>">
            <br>
            <label for="pass">Mot de passe:</label>
            <input id="pass" type="text" name="" placeholder="<?php echo   $donnees[0]['u_pass']  ;  ?>">
            <br>
            <label for="rang">Rang:</label>
            <SELECT name="rang" size="1" id="rang">
                <OPTION selected><?php echo   $donnees[0]['rang']  ;  ?></option>
<?php
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );
        if ( ( $reponse = $bdd->query ( 'SELECT rang.* FROM `rang`;' )  ) !== false ) {
            while (( $donnees = $reponse->fetch( PDO::FETCH_ASSOC ) ) !==false) {
                echo '<OPTION value="' . $donnees['r_id'] . '">' . $donnees['r_libelle']  . '</OPTION>' ;
            }
        $reponse->closeCursor( );
        }
    }   catch( PDOException $e ) {
        die( $e->getMessage( ) );
        }
?>

            </SELECT>
            <input class="button" type="submit">
            <a class="button" href="exo_session_membre_page.php">Annuler</a>
        </form>
        </span>
    </main>
    <footer style="height: 15vh; background-color: lightgrey">

    </footer>
</body>
</html>