<?php
session_start();

if (isset($_POST['login']) && $_POST['login'] != '' && isset($_POST['action'])) {
    try {
        $bdd = new PDO(
            'mysql:host=localhost;dbname=exoadministration;charset=utf8',
            'root',
            '',
            array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
            )
        );

        if ( ( $reponse = $bdd->prepare ( 'SELECT user.u_login FROM `user`
                                            WHERE `u_login` =:login;
            ' )  ) !== false ) {
            if ( $reponse->bindValue( "login", $_POST['login'] ) ) {
                if ( $reponse->execute( ) ) {
                    if ( ( $donnees = $reponse->fetchAll( PDO::FETCH_ASSOC ) ) !==false ) {
                        $exist = false;
                        foreach ($donnees as $key => $value) {
                            if ( $_POST['login'] == $value['u_login'] ) {
                                $exist = true;
                            }
                        }
                            if( $exist===true ) {
                                $_SESSION['login'] = $_POST['login'];
                                if ($_POST['action'] == "modifier" ) {
                                    header('location:exo_session_membre_modify.php');
                                    exit();
                                }elseif ($_POST['action'] == "supprimer" ) {
                                    header('location:exo_session_membre_delete.php');
                                    exit();
                                }
                            }else{
                                $_SESSION['wrongpass'] = "wrong";
                                header('location:exo_session_membre_find.php');
                                exit();
                            }
                    }
                }
            }
            $reponse->closeCursor( );
        }
    } catch( PDOException $e ) {
        die( $e->getMessage( ) );
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
    <title>Recherche</title>
</head>
<body>
    <div class="cadre">
        <form action="" class="form" method="post">

            <input type="text" name="login" placeholder="Login *" id="login">
            <br>
            <label for="login"> * Champs obligatoires</label>
            <hr>
            <input type="radio" name="action" value="modifier" id="modify">
            <label for="modify">Modifier</label>
            <input type="radio" name="action" value="supprimer" id="supp">
            <label for="supp">Supprimer</label>
            <br>
            <input class="button" type="submit" value="Rechercher">
            <br>
            <a class="button" href="exo_session_membre_page.php">ANNULER</a>
            <?php
                if (isset($_SESSION['wrongpass'])) {
                    if ($_SESSION['wrongpass'] == 'wrong') {
                        echo '<br>Login inconnu';
                        unset($_SESSION['wrongpass']);
                    }
                }
            ?>
        </form>
    </div>
</body>
</html>