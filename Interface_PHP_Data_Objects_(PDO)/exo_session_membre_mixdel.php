<?php
session_start();

function checkbox(){
    try {
            $bdd = new PDO(
                'mysql:host=localhost;dbname=exoadministration;charset=utf8',
                'root',
                '',
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
                )
            );

            if ( ( $reponse = $bdd->query ( 'SELECT user.u_login, user.u_id FROM `user`;' )  ) !== false ) {
                if ( ( $donnees = $reponse->fetchAll( PDO::FETCH_ASSOC ) ) !==false ) {
                    foreach ($donnees as $key => $value) {
                        echo '  <input type="checkbox" id="' . $value['u_id'] . '" name="cible[]" value="' . $value['u_id'] . '">
                                        <label for="' . $value['u_id'] . '">' . $value['u_login'] . '</label><br>';
                    }
                }
                $reponse->closeCursor( );
            }
    }   catch( PDOException $e ) {
        die( $e->getMessage( ) );
        }
}



if (isset($_POST['supprimer'])) {
    if (isset($_POST['cible']) && $_POST['cible'] != '' ) {
        try {
            $bdd = new PDO(
                'mysql:host=localhost;dbname=exoadministration;charset=utf8',
                'root',
                '',
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "utf8mb4" COLLATE "utf8mb4_general_ci"'
                )
            );
            foreach ($_POST['cible'] as $key => $value) {
                if ( ( $reponse = $bdd->prepare ( 'DELETE FROM `user`
                                                    WHERE `u_id` =:id;
                    ' )  ) !== false ) {
                    if ( $reponse->bindValue( "id", $value ) ){
                        if ( $reponse->execute( ) ) {

                        }
                    }
                    $reponse->closeCursor( );
                }
            }

        }   catch( PDOException $e ) {
            die( $e->getMessage( ) );
        }
    }
}



?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="exo_session_membre.css">
    <title>Suppression</title>
</head>
<body>
    <header>
        <h1>Suppression de plusieurs utilisateurs</h1>
    </header>
    <form action="" method="post">
        <?php checkbox(); ?>
        <br>
        <input class="button" type="submit" value="Supprimer" name="supprimer">
        <a class="button" href="exo_session_membre_page.php">Annuler</a>
    </form>
</body>
</html>