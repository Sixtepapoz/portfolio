<?php
/**
 * Created by PhpStorm.
 * User: webuser
 * Date: 22/01/2018
 * Time: 11:31
 */

session_start();

//--------------------------------------------------------------------------

/**LoadClass
 * loadClass charge tous les fichiers class depuis une destination définie
 * @param $className
 */
function loadClass( $className ) {
    $_str_file = 'classes/' . strtolower( $className ) . '.class.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );
}

spl_autoload_register( 'loadClass' ); // On lance la procédure d'auto-chargement des classes avec la fonction "loadClass" en callback

/**rollmenu
 *rollmenu génère un menu déroulant
 * @param $statut [optionnel permet de garder en selected la ligne selectionnée]
 */
function rollmenu($statut = false){
    $menu = new PersoModel();
    $combattants = $menu->selectAll();
    foreach ($combattants as $combattant) {
        if ($combattant->getId() == $statut){
            echo '<OPTION selected="selected" style="background-color: bisque" value="' . $combattant->getId() . '">' . $combattant->getNom() . '; PV= ' . $combattant->getDegat() . '</OPTION>';
        }else echo '<OPTION style="background-color: bisque" value="' . $combattant->getId() . '">' . $combattant->getNom() . '; PV= ' . $combattant->getDegat() . '</OPTION>';
    }
}

/**liste
 *liste affiche une liste complète des perso inscrits
 */
function liste() {
    $menu = new PersoModel();
    $combattants = $menu->selectAll();
    foreach ($combattants as $combattant) {
        if ($combattant->getDegat() < 25){
            echo '<li style="background-color: darkred; color: bisque">' . $combattant->getNom() . '; PV= ' . $combattant->getDegat() . '</li>';
        }else  echo '<li style="background-color: bisque">' . $combattant->getNom() . '; PV= ' . $combattant->getDegat() . '</li>';
    }
}

//--------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//
// GESTION DE L'ATTAQUE
//
//--------------------------------------------------------------------------------

if (isset($_POST['attaque'])){
    if (isset($_POST['att']) && $_POST['att'] != ''){
        if (isset($_POST['def']) && $_POST['def'] != ''){
            $persoModel = new PersoModel();
            $att = $persoModel->select($_POST['att']);
            $def = $persoModel->select($_POST['def']);
            $resultat = $att->fight($def);
            if ($def->getDegat() > 0 && $att->getDegat() > 0){
                $persoModel->update($def->getId(), $def->getDegat());
                $persoModel->update($att->getId(), $att->getDegat());
            }
            if ($def->getDegat() <= 0 && $att->getDegat() <= 0){
                $persoModel->delete($def->getID());
                $persoModel->delete($att->getID());
                $resultat = $resultat . " C'est un match nul; les deux combattants sont éliminés...";
            }
            if ($def->getDegat() <= 0){
                $persoModel->delete($def->getID());
                $persoModel->update($att->getId(), $att->getDegat());
                $resultat = $resultat . " " . $def->getNom() . " est éliminé du tournoi. ";
            }
            if ($att->getDegat() <= 0){
                $persoModel->delete($att->getID());
                $persoModel->update($def->getId(), $def->getDegat());
                $resultat = $resultat . " " . $att->getNom() . "est éliminé du tournoi. ";
            }

        }
    }
}

//--------------------------------------------------------------------------------
//
// CREATION DE PERSONNAGE
//
//--------------------------------------------------------------------------------

if (isset($_POST['valid'])){
    if (isset($_POST['nom']) && $_POST['nom'] != ''){
        $menu = new PersoModel();
        if ($menu->create($_POST['nom']) ){
            $_SESSION['resultcreate'] = 'Création de personnage effectuée. A vos armes!';
        }else  $_SESSION['resultcreate'] = 'Problème lors de la création, veuillez réessayer.';
        unset($_POST['nom']);
    }
    unset($_POST['valid']);
    header('Location: index.php');
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css" type="text/css">
    <title>Duelamort</title>
</head>
<body>
    <header>
        <h1>DUEL D'ESCRIME</h1>
        <hr>
    </header>
    <main>
        <form action="" method="post" class="form">
            <label for="nom" style="color: bisque">Nom du personnage</label>
            <input type="text" id="nom" name="nom">

            <input type="submit" class="button" name="valid" value="Créer">
        </form>
        <?php
            if (isset( $_SESSION['resultcreate'] )){
                echo '<span style="background-color: brown">' . $_SESSION['resultcreate'] . '</span>';
                unset($_SESSION['resultcreate']);
            }
        ?>

        <hr>
        <div>
            <form class="form" method="post">
                <label for="att">Attaquant</label>
                <select name="att" id="att">
                    <option value=""></option>
                    <?php
                    if (isset($_POST['att'])){
                    rollmenu($_POST['att']);
                    }else rollmenu();
                    ?>
                </select>
                <label for="def">Défenseur</label>
                <select name="def" id="def">
                    <option value=""></option>
                    <?php
                    if (isset($_POST['def'])){
                        rollmenu($_POST['def']);
                    }else rollmenu();
                    ?>
                </select>
                <input type="submit" class="button" value="Attaquer" name="attaque">
            </form>
            <div class="cadre liste">
                <ul>
                    <?php
                        liste();
                    ?>
                </ul>
            </div>
            <div class="cadre resultat"> <?php echo (isset($resultat)? $resultat:"" ); ?></div>
        </div>
    </main>
    <footer>

    </footer>
</body>
</html>
