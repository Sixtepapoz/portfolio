<?php
/**
 * Created by PhpStorm.
 * User: webuser
 * Date: 22/01/2018
 * Time: 11:34
 */
class Model {
    protected $pdo;

    public function __construct() {
        try {
            $this->pdo = new PDO( 'mysql:host=localhost;dbname=streetfighter;charset=utf8', 'root', '' );
        } catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }
}