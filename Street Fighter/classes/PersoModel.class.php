<?php
/**
 * Created by PhpStorm.
 * User: webuser
 * Date: 22/01/2018
 * Time: 11:35
 */

class PersoModel extends Model {

    /**
     * @return bool
     */
    public function selectAll() {
        try {
            if( ( $donnees = $this->pdo->query( 'SELECT * FROM `perso` ORDER BY `nom` ASC' ) )!==false ) {
                foreach( $donnees->fetchAll( PDO::FETCH_ASSOC ) as $key=>$perso ) {
                    $combattant[$key] = new Perso();
                    $combattant[$key]->setId( $perso['id'] );
                    $combattant[$key]->setNom( $perso['nom'] );
                    $combattant[$key]->setDegat($perso['degat']);
                }
                return $combattant;
            }
            return false;
        } catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }

    /**
     * @param $id
     * @return bool|Perso
     */
    public function select($id) {
        try {
            if( ( $perso = $this->pdo->prepare( 'SELECT * FROM `perso` WHERE `id` = :id' ) )!==false ) {
                if ($perso->bindValue( 'id', $id )) {
                    if ($perso->execute()) {
                        $data = $perso->fetch(PDO::FETCH_ASSOC);
                        $combattant = new Perso();
                        $combattant->setId($data['id']);
                        $combattant->setNom($data['nom']);
                        $combattant->setDegat($data['degat']);
                        return $combattant;

                    }
                }
            }
            return false;
        } catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }

    /**
     * @param $nom
     * @return bool
     */
    public function create($nom){
        try {
            if ( ( $create = $this->pdo->prepare('INSERT INTO `perso` (`nom`) VALUE (:nom) ' ) ) !==false ){
                if ( $create->bindValue( 'nom', $nom ) ){
                    if ( $create-> execute() )
                        return true;
                }
            }
            return false;
        }catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }

    /**
     * @param $id
     * @param $degat
     * @return bool
     */
    public function update($id, $degat){
        try {
            echo "<br>";
            if ( ( $create = $this->pdo->prepare( 'UPDATE `perso` SET `degat` = :degat WHERE `id`= :id') ) !==false ){
                if ( $create->bindValue( 'id', $id ) && $create->bindValue( 'degat', $degat ) ){
                    if ( $create-> execute() )
                        return true;
                }
            }
            return false;
        }catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id){
        try {
            if( ( $create = $this->pdo->prepare( 'DELETE FROM `perso` WHERE `id` = :id') ) !==false ){
                if ( $create->bindValue( 'id', $id ) ){
                    if ( $create-> execute() )
                        return true;
                }
            }
        }catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }
}