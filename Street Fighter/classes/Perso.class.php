<?php
/**
 * Created by PhpStorm.
 * User: Sxt
 * Date: 22/01/2018
 * Time: 11:35
 */
class Perso {
    /**
     * -------------------------------------------------------------
     * PROPERTIES
     * -------------------------------------------------------------
     */

    private $id;
    private $nom;
    private $degat;
    private static $riposte = 0;

    /**
     * -------------------------------------------------------------
     * SETTERS/GETTERS
     * -------------------------------------------------------------
     */

    /**getId
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**setId
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**getNom
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**setNom
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**getDegat
     * @return mixed
     */
    public function getDegat()
    {
        return $this->degat;
    }

    /**setDegat
     * @param mixed $degat
     */
    public function setDegat($degat)
    {
        $this->degat = $degat;
    }

    /**
     * -------------------------------------------------------------
     * METHODS
     * -------------------------------------------------------------
     */

    /**fight
     * Génère la valeur de dégât et renvoi vers receiveDegats
     * @param $def
     * @return mixed
     */
    public function fight($def ){
        $dgt = mt_rand(0,25);
        return $def->receiveDegats($dgt, $this);

    }


    /**receiveDegats
     * Teste l'esquive, la riposte et la fuite pour appliquer les dégâts au défenseur
     * @param $dgt
     * @param $att
     * @return string
     */
    public function receiveDegats($dgt, $att ){
        $dodge = mt_rand(0,100);
        $ripost = mt_rand(0,100);
        $escape = mt_rand(0,100);
        if ($dodge > 15) {
            $this->setDegat($this->getDegat() - $dgt);
            $resultat = $this->getNom() . " a encaissé " . $dgt . ". ";
        }else $resultat = $this->getNom() . " a esquivé avec brio! ";
        if (self::$riposte === 0) {
            self::$riposte = 1;
            if ($ripost <= 8) {
                $this->fight($att);
                $resultat = $resultat . " Il en profite pour riposter! ";
            }
            if ($escape <= 10) {
                $soin = mt_rand(0, 25);
                $this->setDegat($this->getDegat() + $soin);
                $resultat = $resultat . " Une magnifique feinte lui permet de se soigner et de récupérer: " . $soin;
            }
        }else self::$riposte = 0;
        return $resultat;
    }

}