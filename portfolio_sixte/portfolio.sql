SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `portfolio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `portfolio`;

DROP TABLE IF EXISTS `accueil`;
CREATE TABLE IF NOT EXISTS `accueil` (
  `Titre` varchar(25) NOT NULL,
  `Descriptif` longtext NOT NULL,
  PRIMARY KEY (`Titre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `accueil`;
INSERT INTO `accueil` (`Titre`, `Descriptif`) VALUES('Bienvenue', 'Bonjour je suis l''accueil');

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `Titre` varchar(50) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prenom` varchar(50) DEFAULT NULL,
  `Adresse` varchar(255) DEFAULT NULL,
  `Tel` varchar(15) DEFAULT NULL,
  `Mail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Titre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `contact`;
DROP TABLE IF EXISTS `cv`;
CREATE TABLE IF NOT EXISTS `cv` (
  `Section` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) DEFAULT NULL,
  `Descriptif` longtext,
  `Lien` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `cv`;
DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(128) NOT NULL,
  `Lien` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `image`;
DROP TABLE IF EXISTS `mention`;
CREATE TABLE IF NOT EXISTS `mention` (
  `Titre` varchar(50) NOT NULL,
  `Texte` longtext NOT NULL,
  PRIMARY KEY (`Titre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `mention`;
DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `ID_mail` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(128) NOT NULL,
  `Mail` varchar(128) NOT NULL,
  PRIMARY KEY (`ID_mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `newsletter`;
DROP TABLE IF EXISTS `realisation`;
CREATE TABLE IF NOT EXISTS `realisation` (
  `Section` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) DEFAULT NULL,
  `Descriptif` longtext,
  `Lien` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `realisation`;
DROP TABLE IF EXISTS `relation0`;
CREATE TABLE IF NOT EXISTS `relation0` (
  `Section` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`Section`,`ID`),
  KEY `FK_relation0_ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `relation0`;
DROP TABLE IF EXISTS `relation1`;
CREATE TABLE IF NOT EXISTS `relation1` (
  `Section` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`Section`,`ID`),
  KEY `FK_relation1_ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `relation1`;
DROP TABLE IF EXISTS `relation2`;
CREATE TABLE IF NOT EXISTS `relation2` (
  `Section` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`Section`,`ID`),
  KEY `FK_relation2_ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `relation2`;
DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `Section` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) DEFAULT NULL,
  `Descriptif` longtext,
  `Lien` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `service`;
DROP TABLE IF EXISTS `social`;
CREATE TABLE IF NOT EXISTS `social` (
  `Titre` varchar(50) NOT NULL,
  `Lien` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Titre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE `social`;

ALTER TABLE `relation0`
  ADD CONSTRAINT `FK_relation0_ID` FOREIGN KEY (`ID`) REFERENCES `image` (`ID`),
  ADD CONSTRAINT `FK_relation0_Section` FOREIGN KEY (`Section`) REFERENCES `service` (`Section`);

ALTER TABLE `relation1`
  ADD CONSTRAINT `FK_relation1_ID` FOREIGN KEY (`ID`) REFERENCES `image` (`ID`),
  ADD CONSTRAINT `FK_relation1_Section` FOREIGN KEY (`Section`) REFERENCES `realisation` (`Section`);

ALTER TABLE `relation2`
  ADD CONSTRAINT `FK_relation2_ID` FOREIGN KEY (`ID`) REFERENCES `image` (`ID`),
  ADD CONSTRAINT `FK_relation2_Section` FOREIGN KEY (`Section`) REFERENCES `cv` (`Section`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
