<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php echo $pagename; ?></title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header id="header">
        <?php
        if (isset($_SESSION['done'])){
            echo '<div class="modif done">' . $_SESSION['done'] . '</div>';
            unset($_SESSION['done']);
        }elseif (isset($_SESSION['undone'])){
            echo '<div class="modif undone">' . $_SESSION['undone'] . '</div>';
            unset($_SESSION['undone']);
        }

        ?>
        <nav>
            <ul class="nav">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="index.php?c=service">Services</a></li>
                <li><a href="index.php?c=realisation">Réalisations</a></li>
                <li><a href="index.php?c=cv">Curriculum Vitae</a></li>
                <li><a href="index.php?c=contact">Contact</a></li>
                <li>
                    <?php
                    if (isset($_SESSION['user'])){
                        echo '<a href="index.php?deco">Déconnexion</a>';
                    }
                    ?>
                </li>
            </ul>
        </nav>
    </header>