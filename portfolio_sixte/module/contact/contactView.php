<div class="titre">
    <h1><?php echo $affichage['Titre']; ?></h1>
</div>

<aside class="contact">
    <div class="marg">
        <?php echo $affichage['Nom'] . '<br>' . $affichage['Prenom']; ?>
    </div>
    <div class="marg">
        <?php echo $affichage['Adresse']; ?>
    </div>
    <div class="marg">
        <?php echo $affichage['Tel']; ?>
    </div>
    <div class="marg">
        <?php echo $affichage['Mail']; ?>
    </div>
</aside>

<main class="main">
    <h2>Vous avez des question? Contactez-nous.</h2>
    <form action="index.php?c=contact" method="post">
        <label for="mail">Votre adresse mail: *</label>
        <br>
        <input id="mail" type="email" name="expediteur">
        <br>
        <label for="objet">Objet: *</label>
        <br>
        <input class="cadre" type="text" id="objet" name="objet">
        <br>
        <label for="text">Votre question: *</label>
        <br>
        <textarea name="message" id="text" cols="50" rows="10"></textarea>
        <br>
        <input type="submit" name="envoyer" value="Envoyer">
        <p><em>* Champs obligatoire</em></p>
        <?php
            if (isset($_SESSION['err'])){
                echo $_SESSION['err'];
                unset($_SESSION['err']);
            }
        ?>
    </form>
</main>
<hr>