<?php
class contactController {

    private $bd;

    public function __construct(){
        $this->bd = new contactModel;

    }

    public function showAction() {

        if (isset($_POST['expediteur']) && isset($_POST{'objet'})) {
            $headers = "From:noreply@objectif3w.com" . PHP_EOL;
            $headers .= "MIME-Version: 1.0" . PHP_EOL;
            $headers .= "Priority: normal" . PHP_EOL;
            $headers .= "Reply-To: " . htmlentities( $_POST['expediteur'] ) . PHP_EOL;
            //$headers .= "X-Confirm-Reading-To: " . $_POST['expediteur'] . PHP_EOL; // == envoit la confirmation de lecture à "expediteur" (déconseillé)
            //$headers .= "X-Confirm-Receiving-To: " // avis de réception.
            $headers .= "X-Mailer: PHP/" . phpversion() . PHP_EOL; //remplit automatiquement la version PHP
            $headers .= "X-Priority: 3" . PHP_EOL; //Valeurs allant de 1 à 5
            $headers .= "Content-Type: text/plain; charset=utf-8" .PHP_EOL; // texte clair sans HTML
            $headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;
            $headers .= "Cc: " . $_POST['expediteur'] . PHP_EOL; // Copie carbone (toutes les adresses sont visibles)
            //$headers .= "Bcc: direction@objectif3w.com, comptabilite@objectif3w.com, David Fieloux <dfieloux@gmail.com>" . PHP_EOL; // Blind copie carbon (Aucune adresse de visible)

            $message = chunk_split( htmlentities( $_POST['message'] ), 70, PHP_EOL ); //chunk_split permet de couper le texte à "70" caractères
            if( strtoupper( substr( PHP_OS, 0, 3 ) )=='WIN' ) {
                $message = str_replace( "\n.", "\n..", $message); //Permet d'éviter que windows ne supprime un point qui se trouve en début de ligne.
            }

            if (mail( 'spapoz@gmail.com', htmlentities( $_POST['objet'] ), $message, $headers ) ) { //htmlentities empêche le script de s'interpréter.
                $_SESSION['err'] = 'Message envoyé !';
            }else{
                $_SESSION['err'] = 'Une erreur est survenue pendant l\'envoi de l\'email';
            }
        }
        $affichage = $this->bd->SelectAll();
        $pagename = $affichage['Titre'];

        include('header.php');
        include('module/contact/contactView.php');
        include('footer.php');
    }



}