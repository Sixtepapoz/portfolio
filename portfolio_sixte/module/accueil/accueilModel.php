<?php

class accueilModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();

    }


    public function selectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM accueil'))!==false) {
                $result= $statement->fetch(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function updateAcc($title, $text){
        try{
            if ( ($statement = $this->bd-> prepare('UPDATE accueil 
                                                              SET Titre = :title, Descriptif = :text
                                                              '))!==false ){
                if (($statement->bindValue('title', $title) && $statement->bindValue('text', $text))!==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}