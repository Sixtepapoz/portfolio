<?php
class accueilController {

    private $bd;
    private $rea;
    private $limit = 5;

    public function __construct(){
        $this->bd = new accueilModel;
        $this->rea = new realisationModel;
    }

    public function showAction() {
        if (isset($_SESSION['user']) && $_SESSION['user']['rang'] <= '2'){
            if (isset($_POST['modacc'])){
                if (($this->bd->updateAcc($_POST['acctitle'], $_POST['acctext'])) !==false){
                    $_SESSION['done'] = 'Modification appliquée';
                }else $_SESSION['undone'] = 'Problème d\'écriture en base de donnée, essayer à nouveau ou contacter l\'administrateur.' ;
            }
        }
        $realisation = $this->rea->selectLasts($this->limit);
        $affichage = $this->bd->selectAll();
        $pagename = $affichage['Titre'];

        include('header.php');
        include('module/accueil/accueilView.php');
        include('footer.php');
    }


}