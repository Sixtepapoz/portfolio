<main>
    <div class="intro">
        <a href="#page"><div class="arrow"></div></a>
        <div class="citation">
            <p>"Mes logiciels n’ont jamais de bug.<br> Ils développent juste certaines fonctions aléatoires."</p>
        </div>
    </div>
    <div class="paraf">
        <div class="titre" id="page">
        <?php
        if (isset($_SESSION['user']) && $_SESSION['user']['rang'] <= '3'){
            echo '<form action="index.php" method="post"><textarea name="acctitle" id="acctitle" cols="30" rows="1">' . nl2br($affichage['Titre']) . '</textarea><br>';
            echo '<textarea name="acctext" id="acctext" cols="100" rows="10">' . nl2br($affichage['Descriptif']) . '</textarea><br><input type="submit" name="modacc"><br></form>';
        }else{
            echo '<h1>' . $affichage['Titre'] . '</h1>';
            echo '<p>' . $affichage['Descriptif'] . '</p>';
        }

        ?>
        </div>

        <div>
            <div class="titrerea" style="width: 80vw">
                <div>
                    <div class="left imgacc" style="background-image:url(<?php echo $realisation[0]['img'];?>); width: 32vw; height: 48vh"><?php echo $realisation[0]['Titre'];?></div>
                    <div class="right" style="width: 48vw">
                        <div class="imgacc" style="background-image:url(<?php echo $realisation[1]['img'];?>); width: 16vw; height: 16vh"><?php echo $realisation[1]['Titre'];?></div>
                        <div class="imgacc" style="background-image:url(<?php echo $realisation[3]['img'];?>); width: 32vw; height: 16vh"><?php echo $realisation[3]['Titre'];?></div>
                    </div>
                </div>
                <div class="right" style="width: 48vw">
                    <div class="imgacc" style="background-image:url(<?php echo $realisation[2]['img'];?>); width: 32vw; height: 32vh"><?php echo $realisation[2]['Titre'];?></div>
                    <div class="imgacc" style="background-image:url(<?php echo $realisation[4]['img'];?>); width: 32vw; height: 32vh"><?php echo $realisation[4]['Titre'];?></div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
//var_dump($realisation);



