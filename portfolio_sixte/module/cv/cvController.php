<?php
class cvController {

    private $bd;

    public function __construct(){
        $this->bd = new cvModel;
    }

    public function showAction() {
        $affichage = $this->bd->SelectAll();
        $pagename = $affichage['Titre'];

        include('header.php');
        include('module/cv/cvView.php');
        include('footer.php');
    }



}