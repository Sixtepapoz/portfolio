<?php

class serviceModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();

    }


    public function selectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT image.*, image.lien as img, service.* 
                                                           FROM service
                                                           LEFT JOIN relation0 ON service.id_ser = relation0.id_ser
                                                           LEFT JOIN image ON relation0.ID = image.ID
                                                           ORDER BY image.ID ASC 
                                                           '))!==false) {
                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}