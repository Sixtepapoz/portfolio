<?php

class adminModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();

    }


    public function SelectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM accueil'))!==false) {
                $result= $statement->fetch(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function SelectAllNews(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM newsletter'))!==false) {
                $result= $statement->fetch(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function SelectOneNews($id){
        try{
            if ( ($statement = $this->bd-> prepare('SELECT * 
                                                              FROM newsletter 
                                                              WHERE newsletter.ID_mail = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id))!==false){
                    if ($statement->execute()){
                        $result= $statement->fetch(PDO::FETCH_ASSOC);
                        return $result;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function DeleteOneNews($id){
        try{
            if ( ($statement = $this->bd-> prepare('DELETE * 
                                                              FROM newsletter 
                                                              WHERE newsletter.ID_mail = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id))!==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function SelectAllUser(){
        try{
            if ( ($statement = $this->bd->query('SELECT user.*, rank.* FROM user INNER JOIN `rank` ON `rang_fk` = `rang` ORDER BY `rang_fk` ASC '))!==false) {
                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function SelectAllRank(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM rank '))!==false) {
                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function SelectOneUser($id){
        try{
            if ( ($statement = $this->bd-> prepare('SELECT * 
                                                              FROM user 
                                                              WHERE user.id_user = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id))!==false){
                    if ($statement->execute()){
                        $result= $statement->fetch(PDO::FETCH_ASSOC);
                        return $result;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function AddNewUser($login, $pass){
        try{
            if (( $statement = $this->bd->prepare( 'INSERT INTO user (`login`, `pass`, `rang_fk`) VALUES ( :login, :pass, 4)')) !==false){
                if ($statement->bindValue('login', $login) && $statement->bindValue('pass', $pass)){
                    if ($statement->execute()){
                        return $this->bd->lastInsertId();
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function UpdateOneUser($id, $login, $pass){
        try{
            if ( ($statement = $this->bd-> prepare('UPDATE user 
                                                              SET login = :login, pass = :pass
                                                              WHERE user.id_user = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id) && $statement->bindValue('login', $login) && $statement->bindValue('pass', $pass))!==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function UpdateUserRank($id, $rank){
        try{
            if ( ($statement = $this->bd-> prepare('UPDATE user 
                                                              SET rang_fk = :rank
                                                              WHERE user.id_user = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id) && $statement->bindValue('rank', $rank))!==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function DeleteOneUser($id){
        try{
            if ( ($statement = $this->bd-> prepare('DELETE * 
                                                              FROM user 
                                                              WHERE user.id_user = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id))!==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function CheckAccess($login){
        try{
            if ( ($statement = $this->bd-> prepare('SELECT * 
                                                              FROM user 
                                                              INNER JOIN rank ON user.rang_fk = rank.rang
                                                              WHERE user.login = :login
                                                              '))!==false ){
                if (($statement->bindValue('login', $login))!==false){
                    if ($statement->execute()){
                        $result= $statement->fetch(PDO::FETCH_ASSOC);
                        return $result;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}