<?php
class adminController {

    private $bd;

    public function __construct(){
        $this->bd = new adminModel;
    }

    public function loginAction() {
        if (isset($_POST['valid'])){
            if (isset($_POST['pass']) && isset($_POST['log'])){
                $check = $this->bd->CheckAccess($_POST['log']);
                if ((password_verify($_POST['pass'], $check['pass'])) !== false){
                    $_SESSION['user']['log'] = $check['login'];
                    $_SESSION['user']['id'] = $check['id_user'];
                    $_SESSION['user']['rang'] = $check['rang'];
                } else{
                    $_SESSION['wrongpass'] = true;
                }
            }
        }
        $affichage = $this->bd->SelectAll();
        $users = $this->bd->SelectAllUser();
        $rank = $this->bd->SelectAllRank();
        $pagename = 'Section Admin';

        include('header.php');
        include('module/admin/adminView.php');
        include('footer.php');
    }

    public function registerAction() {
        if (isset($_POST['register'])){
            if (empty($check = $this->bd->CheckAccess($_POST['email']))){
                if (isset($_POST['pwd']) && strlen($_POST['pwd']) >= 4 ){
                    if (isset($_POST['email'])){
                        $options = [
                            'cost' => COST,
                        ];
                        $pass = password_hash($_POST['pwd'], PASSWORD_BCRYPT, $options);
                        $last_id = $this->bd->AddNewUser($_POST['email'], $pass);
                        if (($last_id) !==false){
                            $user = $this->bd->SelectOneUser($last_id);
                            $_SESSION['user']['log'] = $user['login'];
                            $_SESSION['user']['id'] = $user['id_user'];
                            $_SESSION['user']['rang'] = $user['rang_fk'] ;
                            $_SESSION['done'] = true;
                        }
                    }
                }
            }else $_SESSION['exist'] = true;
        }
        $affichage = $this->bd->SelectAll();
        $pagename = 'Section Admin';

        include('header.php');
        include('module/admin/adminView.php');
        include('footer.php');

    }

    public function oneUserAction(){
        if (isset($_POST['changer'])){
            if (isset($_POST['rang']) && is_numeric($_POST['rang'])){
                $this->bd->UpdateUserRank($_GET['id'], $_POST['rang']);
            }
        }
        $affichage = $this->bd->SelectAll();
        $users = $this->bd->SelectAllUser();
        $rank = $this->bd->SelectAllRank();
        $user = $this->bd->SelectOneUser($_GET['id']);
        $pagename = 'Section Admin';

        include('header.php');
        include('module/admin/adminView.php');
        include('footer.php');
    }

    public function showAction() {
        $affichage = $this->bd->SelectAll();
        $users = $this->bd->SelectAllUser();
        $rank = $this->bd->SelectAllRank();
        $pagename = 'Section Admin';

        include('header.php');
        include('module/admin/adminView.php');
        include('footer.php');
    }



}