<?php
class socialController {

    private $bd;

    public function __construct(){
        $this->bd = new socialModel;

    }

    public function showAction() {
        $affichage = $this->bd->SelectAll();
        $pagename = $affichage['Titre'];

        include('header.php');
        include('module/social/socialView.php');
        include('footer.php');
    }



}