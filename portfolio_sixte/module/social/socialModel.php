<?php

class socialModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();

    }


    public function SelectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM social'))!==false) {
                $result= $statement->fetch(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}