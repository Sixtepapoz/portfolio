<div class="titre">
    <h1>Newsletter</h1>
</div>
<aside class="contact">
    <form action="index.php?c=newsletter" method="post">
        <h3>Vous désirez vous désinscrire?</h3>
        <label for="email">Votre adresse email: *</label>
        <input type="email" id="email"><br>
        <input type="submit" name="newsdel" value="Désincrire">
    </form>
    <p>
        <?php
        if (isset($_SESSION['del'])){
            echo '<p>' . $_SESSION['err'] . '.</p>';
            unset($_SESSION['del']);
        }
        ?>
    </p>
</aside>
<main class="main">
    <h2>Vous désirez être tenu(e) au courant des dernières évolution du site?</h2>
    <form action="index.php?c=newsletter" method="post">
        <label for="mail">Votre adresse mail: *</label>
        <br>
        <input id="mail" type="email" name="expediteur">
        <br>
        <label for="nom">Nom: *</label>
        <br>
        <input class="cadre" type="text" id="nom" name="nom">
        <br>
        <input type="submit" name="envoyer" value="Envoyer">
        <p><em>* Champs obligatoire</em></p>
        <?php
        if (isset($_SESSION['err'])){
            echo '<p>' . $_SESSION['err'] . '.</p>';
            unset($_SESSION['err']);
        }
        ?>
    </form>
</main>
<hr>