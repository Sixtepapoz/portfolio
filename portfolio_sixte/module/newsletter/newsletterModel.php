<?php

class newsletterModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();

    }


    public function SelectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT * FROM newsletter'))!==false) {
                $result= $statement->fetch(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function deleteMail($mail){
        try{
            if (($statement = $this->bd->prepare('DELETE * FROM newsletter WHERE Mail = :mail')) !==false){
                if (($statement->bindValue('mail', $mail)) !==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }
    }

    public function setMail($mail, $nom){
        try{
            if (($statement = $this->bd->prepare('INSERT INTO newsletter (\'Nom\', \'Mail\') VALUES (:nom, :mail)')) !==false){
                if (($statement->bindValue('mail', $mail) && $statement->bindValue('nom', $nom)) !==false){
                    if ($statement->execute()){
                        return true;
                    }
                }
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }
    }
}