<?php
class newsletterController {

    private $bd;

    public function __construct(){
        $this->bd = new newsletterModel;

    }

    public function showAction() {
        if (isset($_POST['newsdel'])){
            if (isset($_POST['email'])){
                if (($this->bd->deleteMail($_POST['email']))!==false){
                    $_SESSION['del'] = 'Désinscription validée';
                }else{
                    $_SESSION['del'] = 'Erreur lors de la désinscription';
                }
            }
        }
        if (isset($_POST['envoyer'])){
            if (isset($_POST['expediteur']) && isset($_POST['nom'])){
                if (($this->bd->setMail($_POST['expediteur'], $_POST['nom'])) !== false){
                    $_SESSION['err'] = 'Inscription validée';
                }else{
                    $_SESSION['err'] = 'Erreur lors de l\'inscription';
                }
            }
        }
        $affichage = $this->bd->SelectAll();
        $pagename = $affichage['Titre'];

        include('header.php');
        include('module/newsletter/newsletterView.php');
        include('footer.php');
    }



}