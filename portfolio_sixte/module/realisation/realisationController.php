<?php
class realisationController {

    private $bd;

    public function __construct(){
        $this->bd = new realisationModel;
    }

    public function showAction() {
        $pagename = 'Réalisations';
        if (!isset($_GET['id'])){
            $affichage = $this->bd->SelectAll();
        }else{
            $affichage = $this->bd->SelectOne($_GET['id']);
        }

        include('header.php');
        include('module/realisation/realisationView.php');
        include('footer.php');
    }



}