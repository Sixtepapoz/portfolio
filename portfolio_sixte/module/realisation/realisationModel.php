<?php

class realisationModel{

    private $bd;

    public function __construct(){
        $this->bd = SPDO::getInstance()->getDb();
    }


    public function selectAll(){
        try{
            if ( ($statement = $this->bd->query('SELECT image.*, image.lien as img, realisation.* 
                                                           FROM realisation
                                                           LEFT JOIN relation1 ON realisation.id_rea = relation1.id_rea
                                                           LEFT JOIN image ON relation1.ID = image.ID
                                                           GROUP BY realisation.id_rea
                                                           ORDER BY realisation.id_rea ASC
                                                           '))!==false) {
                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function selectOne($id){
        try{
            if ( ($statement = $this->bd-> prepare('SELECT image.*, image.lien as img, realisation.* 
                                                              FROM realisation 
                                                              LEFT JOIN relation1 ON realisation.id_rea = relation1.id_rea
                                                              LEFT JOIN image ON relation1.ID = image.ID
                                                              WHERE realisation.id_rea = :id
                                                              '))!==false ){
                if (($statement->bindValue('id', $id))!==false){
                    if ($statement->execute()){
                        $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                        return $result;
                    }
                }
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function selectLasts($limit){
        try{
            if ( ($statement = $this->bd-> query('SELECT image.*, image.lien as img, realisation.* 
                                                            FROM realisation                                                             
                                                            LEFT JOIN relation1 ON realisation.id_rea = relation1.id_rea
                                                            LEFT JOIN image ON relation1.ID = image.ID
                                                            GROUP BY realisation.id_rea
                                                            ORDER BY realisation.id_rea DESC LIMIT 0, ' . $limit))!==false ){
                        $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                        return $result;
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}