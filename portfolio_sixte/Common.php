<?php
/**
 * Created by PhpStorm.
 * User: webuser
 * Date: 02/02/2018
 * Time: 14:45
 */



/**
 * loadClasses - Checks whether a file exists and includes it
 * @param   string  $className
 * @return
 **/
function loadClass( $className ) {
    $_str_file = 'classes/' . strtolower( $className ) . '.class.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'vendor/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/contact/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/cv/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/image/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/mention/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/newsletter/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/realisation/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/service/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/social/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/accueil/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );

    $_str_file = 'module/admin/' . $className . '.php';
    if( file_exists( $_str_file ) )
        require_once( $_str_file );
}

spl_autoload_register( 'loadClass' ); // On lance la procédure d'auto-chargement des classes avec la fonction "loadClass" en callback

