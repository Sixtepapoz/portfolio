<?php

session_start();

require_once( 'ini.php' );
require_once( 'common.php' );
require_once( 'vendor/SPDO.php' );

if (isset($_GET['deco'])){
    unset($_SESSION['user']);
}


if( !isset( $_GET['c'] ) ) {
    $controller = new accueilController;
    if( !isset( $_GET['a'] ) ) {
        $controller->showAction();
    } else {
        $action = strtolower($_GET['a']) . "Action";
        if (method_exists($controller, $action)) {
            $controller->$action($_POST);
        } else {
            header('Location:404');
        }
    }
} else {
    $control = strtolower($_GET['c']) . "Controller";
    $dosAct = $_GET['c'];
    if (class_exists($control)){
        $controller = new $control;
        if( !isset( $_GET['a'] ) ) {
            $controller->showAction();
        } else {
            $action = strtolower($_GET['a']) . "Action";
            if (method_exists($controller, $action)) {
                $controller->$action($_POST);
            } else {
                header('Location:404');
            }
        }
    }else{
        header('Location:404');
    }
}

